<?php
/**
 * @package FR_WP_Admin_Settings
 * @since   1.1.0
 *
 * Plugin Name:       FR WP Admin Settings
 * Plugin URI:        https://gitlab.com/future-reference/fr-wp-admin-settings
 * Description:       Future Reference WP Admin Settings
 * Version:           1.1.0
 * Author:            Future Reference
 * Author URI:        https://futurereference.co/
 * License:           GPLv2 or later
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * GitLab Plugin URI: https://gitlab.com/future-reference/fr-wp-admin-settings
 */

/**
 * If this file is called directly, abort.
 *
 * @since 1.0.0
 */
if (!defined('WPINC')) {
		die;
}

/**
 * Include the dependencies needed to instantiate the plugin.
 *
 * @since 1.0.0
 */
include_once(plugin_dir_path(__FILE__) . 'shared/class-deserializer.php');

foreach (glob(plugin_dir_path(__FILE__) . 'admin/*.php') as $file) {
		include_once $file;
}

/**
 * Entrypoint for the FR WP Admin Settings plugin.
 *
 * @since 1.1.0
 */
add_action('plugins_loaded', 'fr_wp_admin_settings');
function fr_wp_admin_settings()
{
		$serializer = new FR_WP_Serializer();
		$serializer->init();

		$deserializer = new FR_WP_Deserializer();

		$plugin = new FR_WP_Submenu(new FR_WP_Submenu_Page($deserializer));
		$plugin->init();

		$adminOptions = new FR_WP_Admin_Options($deserializer);
		$headerOptions = new FR_WP_Header_Options($deserializer);
		$loginOptions = new FR_WP_Login_Options($deserializer);
		$tinyMCEOptions = new FR_WP_TinyMCE_Options($deserializer);
		$removeYoastComments = new FR_WP_RemoveYoastComments($deserializer);
}
