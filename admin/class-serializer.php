<?php
/**
 * Performs all sanitization functions required to save the option values to
 * the database.
 *
 * @package FR_WP_Admin_Settings
 * @since   1.0.3
 */

/**
 * Performs all sanitization functions required to save the option values to
 * the database.
 *
 * This will also check the specified nonce and verify that the current user has
 * permission to save the data.
 *
 * @package FR_WP_Admin_Settings
 */
class FR_WP_Serializer
{
	public function init()
	{
		add_action('admin_post', array($this, 'save'));
	}

	public function save()
	{
		// First, validate the nonce and verify the user as permission to save.
		if (!($this->has_valid_nonce() && current_user_can('manage_options'))) {
			// TODO: Display an error message.
		}

		/* Admin Options */

		$value = $_POST['hide_comments'] ? true : false;
		update_option('fr_admin_setting[hide_comments]', $value);

		$value = $_POST['hide_posts'] ? true : false;
		update_option('fr_admin_setting[hide_posts]', $value);

		$value = $_POST['hide_menus'] ? true : false;
		update_option('fr_admin_setting[hide_menus]', $value);

		$value = $_POST['hide_themes'] ? true : false;
		update_option('fr_admin_setting[hide_themes]', $value);

		$value = $_POST['hide_widgets'] ? true : false;
		update_option('fr_admin_setting[hide_widgets]', $value);

		$value = $_POST['hide_customizer'] ? true : false;
		update_option('fr_admin_setting[hide_customizer]', $value);

		/* Header Options */

		$value = $_POST['hide_wp_generator'] ? true : false;
		update_option('fr_admin_setting[hide_wp_generator]', $value);

		$value = $_POST['hide_rsd_link'] ? true : false;
		update_option('fr_admin_setting[hide_rsd_link]', $value);

		$value = $_POST['hide_wlwmanifest_link'] ? true : false;
		update_option('fr_admin_setting[hide_wlwmanifest_link]', $value);

		$value = $_POST['hide_wp_shortlink'] ? true : false;
		update_option('fr_admin_setting[hide_wp_shortlink]', $value);

		$value = $_POST['hide_feed_links'] ? true : false;
		update_option('fr_admin_setting[hide_feed_links]', $value);

		$value = $_POST['hide_emoji_styles'] ? true : false;
		update_option('fr_admin_setting[hide_emoji_styles]', $value);

		$value = $_POST['hide_oembed'] ? true : false;
		update_option('fr_admin_setting[hide_oembed]', $value);

		/* TinyMCE Options */

		if (wp_unslash($_POST['extra_tiny_mce_data']) !== null) {
			update_option('fr_admin_setting[extra_tiny_mce_data]', $_POST['extra_tiny_mce_data']);
		}

		/* Header Options */

		if (wp_unslash($_POST['login_logo_url']) !== null) {
			update_option('fr_admin_setting[login_logo_url]', $_POST['login_logo_url']);
		}

		if ($_POST['login_logo_id'] !== null) {
			update_option('fr_admin_setting[login_logo_id]', $_POST['login_logo_id']);
		}

		if ($_POST['login_logo_filename'] !== null) {
			update_option('fr_admin_setting[login_logo_filename]', $_POST['login_logo_filename']);
		}

		$this->redirect();
	}

	/**
	 * Determines if the nonce variable associated with the options page is set
	 * and is valid.
	 *
	 * @access private
	 * @return boolean False if the field isn't set or the nonce value is invalid;
	 *                 otherwise, true.
	 */
	private function has_valid_nonce()
	{
		// If the field isn't even in the $_POST, then it's invalid.
		if (!isset($_POST['fr_admin_setting_nonce'])) {
			return false;
		}

		$field  = wp_unslash($_POST['fr_admin_setting_nonce']);
		$action = 'fr_admin_setting_save';

		return wp_verify_nonce($field, $action);
	}

	/**
	 * Redirect to the page from which we came (which should always be the
	 * admin page. If the referred isn't set, then we redirect the user to
	 * the login page.
	 *
	 * @access private
	 */
	private function redirect()
	{
		// To make the Coding Standards happy, we have to initialize this.
		if (!isset($_POST['_wp_http_referer'])) {
			$_POST['_wp_http_referer'] = wp_login_url();
		}

		// Sanitize the value of the $_POST collection for the Coding Standards.
		$url = sanitize_text_field(wp_unslash($_POST['_wp_http_referer']));

		// Redirect back to the admin page.
		wp_safe_redirect(urldecode($url));
		exit;
	}
}
