<?php
/**
 * Sets login options
 *
 * @package FR_WP_Admin_Settings
 * @since   1.0.2
 */

class FR_WP_Login_Options
{
	public function __construct($deserializer)
	{
		$this->deserializer = $deserializer;

		/* Custom login styles */
		add_action('login_enqueue_scripts', array($this, 'custom_login'));

		/* Changes login logo URL to site url */
		add_filter('login_headerurl', array($this, 'login_logo_url'));

		/* Changes login logo title attribute to site name  */
		add_filter('login_headertitle', array($this, 'login_logo_url_title'));
	}

	function custom_login()
	{
		/* Adds default styles  */
		wp_enqueue_style('custom_login_css', plugins_url('assets/css/login.css', dirname(__FILE__)));

		/* Add logo if uploaded  */
		$loginLogoUrl = $this->deserializer->get_value('fr_admin_setting[login_logo_url]');
		$logoLogoId = $this->deserializer->get_value('fr_admin_setting[login_logo_id]');

		if (isset($loginLogoUrl)) {
			$imgFileSrc = get_attached_file($logoLogoId);
			$imgData = wp_get_attachment_image_src($logoLogoId, 'full');
			$ratio = 25;
			$fileExt = substr($imgFileSrc, -3);

			// Determine logo ratio for padding -> height / width * 100
			if (isset($imgData[1]) && $imgData[1] > 1 && isset($imgData[2]) && $imgData[2] > 1) {
				// raster image with wp meta data
				$ratioPercent = ($imgData[2] / $imgData[1]) * 100;
				$ratio = $ratioPercent > 72 ? 72 : round($ratioPercent);
			} elseif ($fileExt == 'svg') {
				$svg = simplexml_load_file($imgFileSrc);
				// Get viewbox if svg
				$viewbox = explode(' ', (string) $svg->attributes()->viewBox);
				$ratioPercent = ($viewbox[3] / $viewbox[2]) * 100;
				$ratio = $ratioPercent > 72 ? 72 : round($ratioPercent);
			}

			$custom_css = "
				#login h1 a {
					height: 0;
					text-indent: 101%;
					white-space: nowrap;
					margin-top: 1rem;
					padding: 0 0 $ratio%;
					background-image: url('$loginLogoUrl');
				}
			";
			wp_add_inline_style('custom_login_css', $custom_css);
		}
	}

	function login_logo_url()
	{
			return home_url();
	}

	function login_logo_url_title()
	{
			return get_bloginfo('name');
	}
}
