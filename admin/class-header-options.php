<?php
/**
 * Sets header output options
 *
 * @package FR_WP_Admin_Settings
 * @since   1.0.0
 */

class FR_WP_Header_Options
{
	public function __construct($deserializer)
	{
		$this->deserializer = $deserializer;
		$this->head_cleanup();
	}

	public function head_cleanup()
	{
		// Removes WordPress Generator Meta Tag
		if ($this->deserializer->get_value('fr_admin_setting[hide_wp_generator]')) {
			remove_action('wp_head', 'wp_generator');
		}

		// Removes EditURI/RSD (Really Simple Discovery) link.
		if ($this->deserializer->get_value('fr_admin_setting[hide_rsd_link]')) {
			remove_action('wp_head', 'rsd_link');
		}

		// Removes wlwmanifest (Windows Live Writer) link.
		if ($this->deserializer->get_value('fr_admin_setting[hide_wlwmanifest_link]')) {
			remove_action('wp_head', 'wlwmanifest_link');
		}

		// Removes shortlink.
		if ($this->deserializer->get_value('fr_admin_setting[hide_wp_shortlink]')) {
			remove_action('wp_head', 'wp_shortlink_wp_head');
		}

		if ($this->deserializer->get_value('fr_admin_setting[hide_feed_links]')) {
			//removes feeds
			remove_action('wp_head', 'feed_links', 2);
			//removes comment feed links
			remove_action('wp_head', 'feed_links_extra', 3);
		}

		// Removes Emoji Scripts/styles
		if ($this->deserializer->get_value('fr_admin_setting[hide_emoji_styles]')) {
			remove_action('wp_head', 'print_emoji_detection_script', 7);
			remove_action('wp_print_styles', 'print_emoji_styles');
			remove_action('admin_print_scripts', 'print_emoji_detection_script');
			remove_action('admin_print_styles', 'print_emoji_styles');
		}

		// Turn off oEmbed auto discovery.
		// Don't filter oEmbed results.
		if ($this->deserializer->get_value('fr_admin_setting[hide_oembed]')) {
			remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10);
			remove_action('wp_head', 'wp_oembed_add_discovery_links');
			remove_action('wp_head', 'wp_oembed_add_host_js');
		}
	}
}
