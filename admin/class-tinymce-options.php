<?php
/**
 * Adds extra TinyMCE buttons
 *
 * @package FR_WP_Admin_Settings
 * @since   1.0.0
 */

class FR_WP_TinyMCE_Options
{
	public function __construct($deserializer)
	{
		$this->deserializer = $deserializer;

		/* Adds extra TinyMCE buttons  */
		add_filter('mce_buttons_2', array($this, 'extra_mce_buttons'));
		add_filter('tiny_mce_before_init', array($this, 'extra_mce_before_init'));
	}

	function extra_mce_buttons($buttons)
	{
			array_unshift($buttons, 'styleselect');
			return $buttons;
	}

	function extra_mce_before_init($settings)
	{
			$style_formats = array(
				array(
					'title' => 'Button',
					'selector' => 'a',
					'classes' => 'button'
				),
				array(
					'title' => 'Button - Arrow',
					'selector' => 'a',
					'classes' => 'button arrow'
				),
			);

			$settings['style_formats'] = json_encode($style_formats);
			return $settings;
	}
}
