<?php
/**
 * Creates the submenu page for the plugin.
 *
 * @package FR_WP_Admin_Settings
 * @since   1.0.0
 */

/**
 * Creates the submenu page for the plugin.
 *
 * Provides the functionality necessary for rendering the page corresponding
 * to the submenu with which this page is associated.
 *
 * @package FR_WP_Admin_Settings
 */
class FR_WP_Submenu_Page
{
	public function __construct($deserializer)
	{
		$this->deserializer = $deserializer;

		/* Enqueue Scripts for Uploader Media */
		add_action('admin_enqueue_scripts', 'wp_enqueue_media');
		add_action('admin_enqueue_scripts', array($this, 'loadPluginScripts'));
	}

	public function loadPluginScripts() {
		wp_enqueue_script('fr_media_uploader', plugins_url('assets/js/uploader.js', dirname(__FILE__)));
	}

	/**
	 * This function renders the contents of the page associated with the Submenu
	 * that invokes the render method. In the context of this plugin, this is the
	 * Submenu class.
	 */
	public function render()
	{
		include_once('views/settings.php');
	}
}
