<div class="wrap">

	<div id="icon-options-general" class="icon32"></div>
	<h1><?php esc_attr_e(get_admin_page_title(), 'FR_WP_Admin_Settings'); ?></h1>

	<form method="post" action="<?php echo esc_html(admin_url('admin-post.php')); ?>">

		<div id="">

		</div>

		<div id="poststuff">
			<div id="post-body" class="metabox-holder columns-2">

							<!-- main content -->
				<div id="post-body-content">
					<div class="meta-box-sortables ui-sortable">

						<div class="postbox">

							<div class="handlediv" title="Click to toggle"><br></div>
							<h2 class="hndle">
								<span>
									<?php esc_attr_e('Disable Items in Menu', 'FR_WP_Admin_Settings'); ?>
								</span>
							</h2>

							<div class="inside">
								<p>
									<input type="checkbox"
											name="hide_posts"
											value="1"
											<?php checked(1, $this->deserializer->get_value('fr_admin_setting[hide_posts]')); ?> />
									<label for="hide_posts">Posts</label>
									<br>
									<input type="checkbox"
											name="hide_comments"
											value="1"
											<?php checked(1, $this->deserializer->get_value('fr_admin_setting[hide_comments]')); ?> />
									<label for="hide_comments">Comments</label>
								</p>
							</div>
									<!-- .inside -->
						</div>

						<div class="postbox">

							<div class="handlediv" title="Click to toggle"><br></div>
							<h2 class="hndle">
								<span>
									<?php esc_attr_e('Disable Items for Editor Roles', 'FR_WP_Admin_Settings'); ?>
								</span>
							</h2>

							<div class="inside">
								<p>
									<input type="checkbox"
											name="hide_menus"
											value="1"
											<?php checked(1, $this->deserializer->get_value('fr_admin_setting[hide_menus]')); ?> />
									<label for="hide_menus">Menus</label>
									<br>
									<input type="checkbox"
											name="hide_themes"
											value="1"
											<?php checked(1, $this->deserializer->get_value('fr_admin_setting[hide_themes]')); ?> />
									<label for="hide_themes">Themes</label>
									<br>
									<input type="checkbox"
											name="hide_widgets"
											value="1"
											<?php checked(1, $this->deserializer->get_value('fr_admin_setting[hide_widgets]')); ?> />
									<label for="hide_widgets">Widgets</label>
									<br>
									<input type="checkbox"
											name="hide_customizer"
											value="1"
											<?php checked(1, $this->deserializer->get_value('fr_admin_setting[hide_customizer]')); ?> />
									<label for="hide_customizer">Customizer</label>
								</p>
							</div>
									<!-- .inside -->
						</div>

						<div class="postbox">

							<div class="handlediv" title="Click to toggle"><br></div>
							<h2 class="hndle">
								<span>
									<?php esc_attr_e('Disable Items in Head', 'FR_WP_Admin_Settings'); ?>
								</span>
							</h2>

							<div class="inside">

								<p>
									<input type="checkbox"
											name="hide_wp_generator"
											value="1"
											<?php checked(1, $this->deserializer->get_value('fr_admin_setting[hide_wp_generator]')); ?> />
									<label for="hide_wp_generator">WP Generator</label>
									<br>
									<input type="checkbox"
											name="hide_rsd_link"
											value="1"
											<?php checked(1, $this->deserializer->get_value('fr_admin_setting[hide_rsd_link]')); ?> />
									<label for="hide_rsd_link">RSD Links</label>
									<br>
									<input type="checkbox"
											name="hide_wlwmanifest_link"
											value="1"
											<?php checked(1, $this->deserializer->get_value('fr_admin_setting[hide_wlwmanifest_link]')); ?> />
									<label for="hide_wlwmanifest_link">Windows Live Writer Manifest Link</label>
									<br>
									<input type="checkbox"
											name="hide_wp_shortlink"
											value="1"
											<?php checked(1, $this->deserializer->get_value('fr_admin_setting[hide_wp_shortlink]')); ?> />
									<label for="hide_wp_shortlink">Shortlink for Posts</label>
									<br>
									<input type="checkbox"
											name="hide_feed_links"
											value="1"
											<?php checked(1, $this->deserializer->get_value('fr_admin_setting[hide_feed_links]')); ?> />
									<label for="hide_feed_links">RSS Feed Links</label>
									<br>
									<input type="checkbox"
											name="hide_emoji_styles"
											value="1"
											<?php checked(1, $this->deserializer->get_value('fr_admin_setting[hide_emoji_styles]')); ?> />
									<label for="hide_emoji_styles">Emoji Styles/Scripts</label>
									<br>
									<input type="checkbox"
											name="hide_oembed"
											value="1"
											<?php checked(1, $this->deserializer->get_value('fr_admin_setting[hide_oembed]')); ?> />
									<label for="hide_oembed">oEmbed auto discovery</label>
								</p>
							</div>
									<!-- .inside -->
						</div>

						<div class="postbox">

							<div class="handlediv" title="Click to toggle"><br></div>
							<h2 class="hndle">
								<span>
									<?php esc_attr_e('Login Logo Url', 'FR_WP_Admin_Settings'); ?>
								</span>
							</h2>

							<div class="inside">
								<p>
									<input type="hidden"
													id="login_logo_id"
													name="login_logo_id"
													value="<?php echo esc_attr($this->deserializer->get_value('fr_admin_setting[login_logo_id]')); ?>" />
									<input type="hidden"
													id="login_logo_url"
													name="login_logo_url"
													value="<?php echo esc_url($this->deserializer->get_value('fr_admin_setting[login_logo_url]')); ?>" />
									<input type="text"
													id="login_logo_filename"
													name="login_logo_filename"
													value="<?php echo esc_attr($this->deserializer->get_value('fr_admin_setting[login_logo_filename]')); ?>" />
									<a id="upload_login_logo_button" class="button-secondary" onclick="upload_image('login_logo_url', 'login_logo_id', 'login_logo_filename')">Upload Login Logo</a>
								</p>
							</div>
									<!-- .inside -->
						</div>
						<!-- .postbox -->

					</div>
					<!-- .meta-box-sortables .ui-sortable -->
				</div>
				<!-- post-body-content -->

				<!-- sidebar -->
				<div id="postbox-container-1" class="postbox-container">

					<div class="meta-box-sortables">

						<div class="postbox">

							<h2><?php esc_attr_e('Update Settings', 'WpAdminStyle'); ?></h2>

							<div class="inside">
								<?php
									wp_nonce_field('fr_admin_setting_save', 'fr_admin_setting_nonce');
									submit_button();
								?>
							</div>
							<!-- .inside -->
						</div>
						<!-- .postbox -->
					</div>
					<!-- .meta-box-sortables -->
				</div>
				<!-- #postbox-container-1 .postbox-container -->

			</div>
		</div>

	</form>

</div>
