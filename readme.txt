=== Future Reference - WordPress Admin Settings ===
Contributors: analogmemory
Tags: admin, settings
Requires at least: 4.0
Tested up to: 4.9.2
Stable tag: 4.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Adds a custom admin section to the WordPress for admins of Future Reference
built sites to control changes.

== Description ==

Adds a custom admin section to the WordPress for admins of Future Reference
built sites to control changes.

* Allows control of which item in admin menu are shown
* Allows control of which items/links are shown in the head
* Add SVG support for media library
* Adds ability to upload custom logo for login page
* Adds a custom html 'A' (with a class of 'button') element for TinyMCE
* Add functionality to remove Yoast SEO Comments from HTML output
