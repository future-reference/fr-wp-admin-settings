var uploader;
function upload_image (url_id, object_id, fileNameId) {

  /* Extend the wp.media object */
  uploader = wp.media.frames.file_frame = wp.media({
    title: 'Choose Image',
    button: {
      text: 'Choose Image'
    },
    multiple: false
  });

  //When a file is selected, grab the URL and set it as the text field's value
  uploader.on('select', function() {
    attachment = uploader.state().get('selection').first().toJSON();
    var url = attachment['url'];
    var objectId = attachment['id'];
    var fileName = attachment['filename'];
    jQuery('#'+url_id).val(url);
    jQuery('#'+object_id).val(objectId);
    jQuery('#'+fileNameId).val(fileName);
  });

  /* Open the uploader dialog */
  uploader.open();
}
